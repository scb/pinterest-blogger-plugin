$(function(){
	var button = $('<a href="#" target="_blank"></a>')
			.css({'display': 'none', 'position': 'absolute', 'z-index': 10000})
			.width(72).height(72)
			.css('background-image', 'url("http://3.bp.blogspot.com/-y3xzTGiGzH0/UK4XOaUpdaI/AAAAAAAADw8/Z1MH4Jr4Efo/s1600/pinterestx1_72.png")')
			.mouseenter( function(){ clearTimeout(hideTimer);}).mouseleave(hideButton)
			.appendTo('body');
	var hideTimer = null;

	var hideButton = function()
	{
		hideTimer = setTimeout( function(){button.fadeOut();}, 250 );
	}

	$('.main img').mouseenter( function(){
		var img = $(this);
		var left = img.offset().left;
		var top = img.offset().top;
		var width = img.outerWidth();

		var url = img.attr('src');
		var title = img.attr('alt') || '';

		var buttonUrl = 'http://pinterest.com/pin/create/button/?url=' + encodeURIComponent( location.href ) + '&media=' + encodeURIComponent( url ) + '&description=' + encodeURIComponent( title );

		button.css('left', left + width - 92).css('top', top + 20).fadeIn().attr('href', buttonUrl);
	}).mouseleave(hideButton);
});